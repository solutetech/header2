package com.example.vincent.header2.adapter;

/**
 * Created by vincent on 12/2/16.
 */


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vincent.header2.CharacterViewHolder;
import com.example.vincent.header2.DragonBallHeader;
import com.example.vincent.header2.HeaderRecyclerViewAdapter;
import com.example.vincent.header2.HeaderViewHolder;
import com.example.vincent.header2.R;
import com.example.vincent.header2.model.Image;


/**
 * Created by Lincoln on 31/03/16.
 */

public class GalleryAdapter extends HeaderRecyclerViewAdapter<RecyclerView.ViewHolder, DragonBallHeader, Image>{

    private static final String LOG_TAG = GalleryAdapter.class.getSimpleName();

    @Override
    protected RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = getLayoutInflater(parent);
        View headerView = inflater.inflate(R.layout.row_dragon_ball_header, parent, false);
        return new HeaderViewHolder(headerView);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = getLayoutInflater(parent);
        View characterView = inflater.inflate(R.layout.gallery_thumbnail, parent, false);
        return new CharacterViewHolder(characterView);
    }


    @Override protected void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        DragonBallHeader header = getHeader();
        HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
        headerViewHolder.render(header);
    }

    @Override protected void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        Image character = getItem(position);
        CharacterViewHolder characterViewHolder = (CharacterViewHolder) holder;
        characterViewHolder.render(character);
    }

    private LayoutInflater getLayoutInflater(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext());
    }

    @Override protected void onHeaderViewRecycled(RecyclerView.ViewHolder holder) {
        Log.v(LOG_TAG, "onHeaderViewRecycled(RecyclerView.ViewHolder holder)");
    }

    @Override protected void onItemViewRecycled(RecyclerView.ViewHolder holder) {
        Log.v(LOG_TAG, "onItemViewRecycled(RecyclerView.ViewHolder holder)");
    }
}