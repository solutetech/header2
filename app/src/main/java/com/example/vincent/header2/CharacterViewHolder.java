package com.example.vincent.header2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vincent.header2.model.Image;
//import com.squareup.picasso.Picasso;

import static com.example.vincent.header2.R.id.coupon_image;

/**
 * Created by vincent on 1/11/17.
 */

public class CharacterViewHolder extends RecyclerView.ViewHolder{

    private final ImageView photoImageView;
    private final Context mContext;


    public CharacterViewHolder(View itemView) {
        super(itemView);
        this.mContext = itemView.getContext();
        this.photoImageView = (ImageView) itemView.findViewById(coupon_image);
    }

    public void render(Image character) {
        String photo = character.getMedium();

        Glide.with(mContext).load(photo)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(photoImageView);
    }
}
