package com.example.vincent.header2.model;

/**
 * Created by vincent on 12/2/16.
 */

import java.io.Serializable;


public class Image implements Serializable{
    private String name;
    private int numOfSongs;
    private int thumbnail;
    private String small, medium, large;
    private String timestamp;
    private String header;

    public Image() {
    }

    public Image(String name, int   numOfSongs, int thumbnail, String small, String medium, String large, String timestamp, String header) {
        this.name = name;
        this.numOfSongs = numOfSongs;
        this.thumbnail = thumbnail;
        this.small = small;
        this.medium = medium;
        this.large = large;
        this.timestamp = timestamp;
        this.header = header;
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfSongs() {return numOfSongs; }

    public void setNumOfSongs(int numOfSongs) { this.numOfSongs = numOfSongs; }

    public int  getThumbnail() {return thumbnail; }

    public void setThumbnail(int thumbnail) {this.thumbnail = thumbnail; }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

}