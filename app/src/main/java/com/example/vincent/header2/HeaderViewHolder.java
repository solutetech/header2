package com.example.vincent.header2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vincent.header2.model.Image;

import java.util.List;

/**
 * Created by vincent on 1/11/17.
 */

public class HeaderViewHolder extends RecyclerView.ViewHolder{

    private final ImageView backDrop;
    private Context context;

    public HeaderViewHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();
        this.backDrop = (ImageView) itemView.findViewById(R.id.backdrop);
    }

    public void render(DragonBallHeader header) {
        List<Image> largePhoto = header.getLarge();
        Glide.with(context).load(largePhoto)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(backDrop);

    }
}
