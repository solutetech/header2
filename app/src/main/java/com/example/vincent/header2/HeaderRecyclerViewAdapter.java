package com.example.vincent.header2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vincent.header2.adapter.GalleryAdapter;
import com.example.vincent.header2.model.Image;

import java.util.Collections;
import java.util.List;

/**
 * Created by vincent on 1/11/17.
 */

public abstract class HeaderRecyclerViewAdapter<VH extends RecyclerView.ViewHolder, H, T>
        extends RecyclerView.Adapter<VH> {

    protected static final int TYPE_HEADER = -2;
    protected static final int TYPE_ITEM = -1;
//    protected static final int TYPE_FOOTER = -3;

    private H header;
    private List<T> items = Collections.EMPTY_LIST;
    private boolean showFooter = true;
    private Context mContext;
    private List<Image> images;
    private ImageView working;


    /**
     * Invokes onCreateHeaderViewHolder, onCreateItemViewHolder or onCreateFooterViewHolder methods
     * based on the view type param.
     */
    @Override public final VH onCreateViewHolder(ViewGroup parent, int viewType) {
        VH viewHolder;
        if (isHeaderType(viewType)) {
            viewHolder = onCreateHeaderViewHolder(parent, viewType);
        } else {
            viewHolder = onCreateItemViewHolder(parent, viewType);
        }
        return viewHolder;
    }


    /**
     * If you don't need header feature, you can bypass overriding this method.
     */
    protected VH onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    protected abstract VH onCreateItemViewHolder(ViewGroup parent, int viewType);

    /**
     * If you don't need footer feature, you can bypass overriding this method.
     */

    /**
     * Invokes onBindHeaderViewHolder, onBindItemViewHolder or onBindFooterViewHOlder methods based
     * on the position param.
     */
    @Override public final void onBindViewHolder(VH holder, int position) {
        if (isHeaderPosition(position)) {
            onBindHeaderViewHolder(holder, position);
        }  else {
            onBindItemViewHolder(holder, position);
        }
    }

    /**
     * If you don't need header feature, you can bypass overriding this method.
     */
    protected void onBindHeaderViewHolder(VH holder, int position) {

    }

    protected abstract void onBindItemViewHolder(VH holder, int position);

    /**
     * If you don't need footer feature, you can bypass overriding this method.
     */

    /**
     * Invokes onHeaderViewRecycled, onItemViewRecycled or onFooterViewRecycled methods based
     * on the holder.getAdapterPosition()
     */
    @Override public final void onViewRecycled(VH holder) {
        int position = holder.getAdapterPosition();

        if (isHeaderPosition(position)) {
            onHeaderViewRecycled(holder);
        } else {
            onItemViewRecycled(holder);
        }
    }

    protected void onHeaderViewRecycled(VH holder) {
    }

    protected void onItemViewRecycled(VH holder) {
    }


    /**
     * Returns the type associated to an item given a position passed as arguments. If the position
     * is related to a header item returns the constant TYPE_HEADER or TYPE_FOOTER if the position is
     * related to the footer, if not, returns TYPE_ITEM.
     *
     * If your application has to support different types override this method and provide your
     * implementation. Remember that TYPE_HEADER, TYPE_ITEM and TYPE_FOOTER are internal constants
     * can be used to identify an item given a position, try to use different values in your
     * application.
     */
    @Override public int getItemViewType(int position) {
        int viewType = TYPE_ITEM;
        if (isHeaderPosition(position)) {
            viewType = TYPE_HEADER;
        }
        return viewType;
    }

    /**
     * Returns the items list size if there is no a header configured or the size taking into account
     * that if a header or a footer is configured the number of items returned is going to include
     * this elements.
     */
    @Override public int getItemCount() {
        int size = items.size();
        if (hasHeader()) {
            size++;
        }

        return size;
    }

    /**
     * Get header data in this adapter, you should previously use {@link #setHeader(H header)}
     * in the adapter initialization code to set header data.
     *
     * @return header data
     */
    public H getHeader() {
        return header;
    }

    /**
     * Get item data in this adapter with the specified postion,
     * you should previously use {@link #setHeader(H header)}
     * in the adapter initialization code to set header data.
     *
     * @return item data in the specified postion
     */
    public T getItem(int position) {
        if (hasHeader() && hasItems()) {
            --position;
        }
        return items.get(position);
    }

    /**
     * Get footer data in this adapter, you should previously use {@link #setFooter(F footer)}
     * in the adapter initialization code to set footer data.
     *
     * @return footer data
     */

    /**
     * If you need a header, you should set header data in the adapter initialization code.
     *
     * @param header header data
     */
    public void setHeader(H header) {
        this.header = header;
    }

    /**
     * You should set header data in the adapter initialization code.
     *
     * @param items item data list
     */
    public void setItems(List<T> items) {
//        validateItems(items);
        this.items = items;
    }

    /**
     * If you need a footer, you should set footer data in the adapter initialization code.
     */

    /**
     * Call this method to show hiding footer.
     */
    public void showFooter() {
        this.showFooter = true;
        notifyDataSetChanged();
    }

    /**
     * Call this method to hide footer.
     */
    public void hideFooter() {
        this.showFooter = false;
        notifyDataSetChanged();
    }

    /**
     * Returns true if the position type parameter passed as argument is equals to 0 and the adapter
     * has a not null header already configured.
     */
    public boolean isHeaderPosition(int position) {
        return hasHeader() && position == 0;
    }

    /**
     * Returns true if the position type parameter passed as argument is equals to
     * <code>getItemCount() - 1</code>
     * and the adapter has a not null header already configured.
     */

    /**
     * Returns true if the view type parameter passed as argument is equals to TYPE_HEADER.
     */
    protected boolean isHeaderType(int viewType) {
        return viewType == TYPE_HEADER;
    }

    /**
     * Returns true if the view type parameter passed as argument is equals to TYPE_FOOTER.
     */
//    protected boolean isFooterType(int viewType) {
//        return viewType == TYPE_FOOTER;
//    }

    /**
     * Returns true if the header configured is not null.
     */
    protected boolean hasHeader() {
        return getHeader() != null;
    }

    /**
     * Returns true if the footer configured is not null.
     */

    /**
     * Returns true if the item configured is not empty.
     */
    private boolean hasItems() {
        return items.size() > 0;
    }

    private void validateItems(List<T> items) {
        if (items == null) {
            throw new IllegalArgumentException("You can't use a null List<Item> instance.");
        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

//

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private HeaderRecyclerViewAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final HeaderRecyclerViewAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
