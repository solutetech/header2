package com.example.vincent.header2;

import com.example.vincent.header2.model.Image;

import java.util.List;

/**
 * Created by vincent on 1/11/17.
 */

public class DragonBallHeader {

    private final List<Image> dragonHead;

    public DragonBallHeader(List<Image> dragonHead) {
        this.dragonHead = dragonHead;
    }

    public List<Image> getLarge() {
        return dragonHead;
    }
}
