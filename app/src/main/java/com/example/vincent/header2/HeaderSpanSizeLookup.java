package com.example.vincent.header2;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by vincent on 1/11/17.
 */

public class HeaderSpanSizeLookup extends GridLayoutManager.SpanSizeLookup{

    private final HeaderRecyclerViewAdapter mAdapter;
    private final GridLayoutManager layoutManager;

    public HeaderSpanSizeLookup(HeaderRecyclerViewAdapter adapter, RecyclerView.LayoutManager layoutManager) {
        this.mAdapter = adapter;
        this.layoutManager = (GridLayoutManager) layoutManager;
    }

    @Override public int getSpanSize(int position) {
        boolean isHeaderOrFooter =
                mAdapter.isHeaderPosition(position);
        return isHeaderOrFooter ? layoutManager.getSpanCount() : 1;
    }
}
