package com.example.vincent.header2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.vincent.header2.adapter.GalleryAdapter;
import com.example.vincent.header2.app.AppController1;
import com.example.vincent.header2.model.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    private static final String endpoint = "http://delaroystudios.com/glide/json/glideimages.json";
    private ArrayList<Image> images;
    private ProgressDialog pDialog;
    private HeaderRecyclerViewAdapter mAdapter;
    private RecyclerView recyclerView;
    private ImageView profilePic;
    private ArrayList<DragonBallHeader> dragonBallHeaders;

    private View footer;

    private Slide slide;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



//        // Click listener for nav footer.
//        footer = findViewById(R.id.userProfilePage);
//        footer.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//
//                footer = findViewById(R.id.userProfilePage);
//                footer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent a = new Intent(getApplicationContext(), Barcode.class);
//                        startActivity(a);
//                    }
//                });
//                footer = findViewById(R.id.userCoupons);
//                footer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Do footer action
//                        Intent t = new Intent(getApplicationContext(), Barcode.class);
//                        startActivity(t);
//                    }
//                });
//                footer = findViewById(R.id.couponTimeline);
//                footer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Do footer action
//                        Intent r = new Intent(getApplicationContext(), Barcode.class);
//                        startActivity(r);
//                    }
//                });
//                footer = findViewById(R.id.userFindButton);
//                footer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
//            }
//        });




        Intent v = getIntent();
        Intent t = getIntent();

        profilePic = (ImageView)findViewById(R.id.backdrop);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        pDialog = new ProgressDialog(this);
        images = new ArrayList<>();
        dragonBallHeaders = new ArrayList<>();
        mAdapter = new GalleryAdapter();

        GridLayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        HeaderSpanSizeLookup headerSpanSizeLookup = new HeaderSpanSizeLookup(mAdapter, mLayoutManager);
        mLayoutManager.setSpanSizeLookup(headerSpanSizeLookup);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {


            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");

            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        prepareAlbums();
        fillRecyclerView();


    }

    private void fillRecyclerView() {
        List<Image> imageList = images;
        DragonBallHeader header = getHeader();
        mAdapter.setHeader(header);
        mAdapter.setItems(imageList);
        mAdapter.notifyDataSetChanged();
    }

//    private List<Image> getDragonHead() {
//        List<Image> header = new ArrayList<>();
//        header.add(new Image());
//
//        return header;
//    }

    private DragonBallHeader getHeader(List<Image> imageList12) {
        profilePic = (ImageView) findViewById(R.id.backdrop);
        for (Image image12345 : imageList12) {
            profilePic.
        }



        return new DragonBallHeader();
    }



    private void prepareAlbums() {
        JsonArrayRequest req = new JsonArrayRequest(endpoint,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
//                        pDialog.hide();


                        images.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Image coupons = new Image();
                                coupons.setName(object.getString("name"));

                                JSONObject url = object.getJSONObject("url");
                                coupons.setSmall(url.getString("small"));
                                coupons.setMedium(url.getString("medium"));
                                coupons.setLarge(url.getString("large"));
                                coupons.setTimestamp(object.getString("timestamp"));

                                try {
                                    profilePic.setImageResource(Integer.parseInt(url.getString("large")));
                                }catch (Exception e) {
                                    e.printStackTrace();
                                }


                                images.add(coupons);

                            } catch (JSONException e) {
                                Log.e(TAG, "Json parsing error: " + e.getMessage());
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
//                pDialog.hide();
            }
        });

        // Adding request to request queue
        AppController1.getInstance().addToRequestQueue(req);
    }





}

